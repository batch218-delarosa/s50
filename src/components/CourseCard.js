import {Row, Col , Card, Button} from 'react-bootstrap';

export default function CourseCard() {
    return (
        <Card className="cardHighlight p-3 mb-3">
	        <Card.Body>
	            <Card.Title>
	                <h2>Sample Course</h2>
	            </Card.Title>
	            <Card.Text>
                    <h5>Description:</h5>
                    <p>This is a sample course offering</p>

                    <p className="m-0">Price: </p>
                    <p>PhP 40,000</p>
	            </Card.Text>

                <Button>Enroll</Button>
	        </Card.Body>
	    </Card>
    )
}